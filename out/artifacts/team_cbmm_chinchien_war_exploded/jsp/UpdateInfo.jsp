<%--
  Created by IntelliJ IDEA.
  User: Chiron
  Date: 23/05/18
  Time: 11:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%--my css--%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/updateInfo.css">
    <link rel="stylesheet" type="text/css" href="../css/website.css"/>
    <%--js--%>
    <script src="./js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <%--summernote--%>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>


    <link rel="stylesheet" type="text/css" href="css/website.css"/>
</head>
<body class="bodyStyle">
<header>
    <%@ include file="navbar.jsp" %>
</header>

<form action="UpdateInfoServlet" Method="POST"  enctype="multipart/form-data">
    <div class="container">
        <h1>Update Information</h1>
        <hr>
        <label for="userName"><b>User Name: ${userInfo.username}</b></label>
        <input type="hidden" placeholder="Your user name here" name="username" id="userName" value="${userInfo.username}">
        <br>
        <%--<label for="psw"><b>Password</b></label>--%>
        <%--<input type="password" placeholder="Enter Password" name="hashed_code" id="psw" required>--%>

        <%--<label for="psw-repeat"><b>Repeat Password</b></label>--%>
        <%--<input type="password" placeholder="Repeat Password" name="hashed_code" id="psw-repeat"  required>--%>

        <label for="fname"><b>First Name</b></label>
        <input type="text" placeholder="Your first name here" name="fname" id="fname" value="${userInfo.fname}"
               required>

        <label for="lname"><b>Last Name</b></label>
        <input type="text" placeholder="Your last name here" name="lname" id="lname" value="${userInfo.lname}" required>

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter Email" name="email" id="email" value="${userInfo.email}">

        <label for="dob"><b>Date of Birth</b></label>
        <div>
            <input type="date" name="dob" id="dob" value="${userInfo.dob}" required>
        </div>
        <br>
        <%@ include file="countryselection.jsp" %>
        <br>
        <div>
            <label for="userDescr"><b>Description</b></label>
            <div id="userDescr" name="descrp">
            <textarea rows="5" cols="50" placeholder="${userInfo.descrp}">
            </textarea>
            </div>
        </div>
        <table>
            <tr>
                <td><img src="image/avatar_default.png" width="75"></td>
                <td><img src="image/avatar01.png" width="75"></td>
                <td><img src="image/avatar02.png" width="75"></td>
                <td><img src="image/avatar03.png" width="75"></td>
                <td><img src="image/avatar04.png" width="75"></td>
                <td><img src="image/avatar05.png" width="75"></td>
            </tr>
            <tr>
                <td>
                    <input type="radio" id="avatar_default" name="avatar" value="avatar_default.png">
                    <label for="avatar_default"> Default</label>
                </td>
                <td>
                    <input type="radio" id="avatar01" name="avatar" value="avatar01.png">
                    <label for="avatar01"> Avatar 1</label>
                </td>
                <td>
                    <input type="radio" id="avatar02" name="avatar" value="avatar02.png">
                    <label for="avatar02">Avatar 2</label>
                </td>
                <td>
                    <input type="radio" id="avatar03" name="avatar" value="avatar03.png">
                    <label for="avatar03">Avatar 3</label>
                </td>
                <td>
                    <input type="radio" id="avatar04" name="avatar" value="avatar04.png">
                    <label for="avatar04">Avatar 4</label>
                </td>
                <td>
                    <input type="radio" id="avatar05" name="avatar" value="avatar05.png">
                    <label for="avatar05">Avatar 5</label>
                </td>
            </tr>
        </table>
        <h3>Or Choose an image to upload:</h3>
        <input type="radio" id="myImg" name="avatar" value="myImg">
        <input name="uploadImg" type="file">
        <br>
        <hr>
        <button type="submit" class="registerbtn" style="font-size:120%;">Update</button>
    </div>
    <span id="msg"></span>
    <c:choose>
        <c:when test="${status != null}">
            <script>
                alert("done");
            </script>
        </c:when>
    </c:choose>

    <%--to check radio button for avatar--%>
    <c:choose>
        <c:when test="${userInfo.avatar == 'avatar_default.png'}">
            <script>
                document.getElementById("avatar_default").checked = true;
            </script>
        </c:when>
        <c:when test="${userInfo.avatar == 'avatar01.png'}">
            <script>
                document.getElementById("avatar01").checked = true;
            </script>
        </c:when>
        <c:when test="${userInfo.avatar == 'avatar02.png'}">
            <script>
                document.getElementById("avatar02").checked = true;
            </script>
        </c:when>
        <c:when test="${userInfo.avatar == 'avatar03.png'}">
            <script>
                document.getElementById("avatar03").checked = true;
            </script>
        </c:when>
        <c:when test="${userInfo.avatar == 'avatar04.png'}">
            <script>
                document.getElementById("avatar04").checked = true;
            </script>
        </c:when>
        <c:when test="${userInfo.avatar == 'avatar05.png'}">
            <script>
                document.getElementById("avatar05").checked = true;
            </script>
        </c:when>
        <c:otherwise>
            <script>
                document.getElementById("myImg").checked = true;
            </script>
        </c:otherwise>
    </c:choose>


</form>

</body>
</html>
