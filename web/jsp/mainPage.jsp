<%@ page import="java.util.List" %>
<%@ page import="DAOs.Article" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="DAOs.User" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main Page</title>

    <%@include file="bcss.jsp"%>

    <%--<script type="text/javascript" src="../css/jquery.min.js"></script>--%>
    <%--summernote--%>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <%--my css--%>
    <link rel="stylesheet" type="text/css" href="css/mainPage.css"/>
    <link rel="stylesheet" type="text/css" href="css/website.css"/>

</head>

<body class="bodyStyle">
    <%@ include file="navbar.jsp" %>
<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="image/image01.png" alt="Technology" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Technology</h3>
                    <p>Technology is connecting the World at a speed of 'speed of 'light' '</p>
                </div>
            </div>

            <div class="item">
                <a href="http://www.ictgraduateschool.ac.nz/" target="_blank"><img src="image/image04.png" alt="Health"
                                                                                   style="width:100%;"></a>
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="image/image03.png" alt="Education" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Education</h3>
                    <p>Learning new things 'FastPace' is like taking an daily dose of Adrenaline!!</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="tab1">
        <%--<div class="col-md-12 row">--%>
        <div class="col-md-12 row getRidOfpad">
            <form class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                <div class="col-md-12">
                    <input class="col-md-9 searchArticle" name="command" type="text"
                           placeholder="Search article or author.." class="allInput" value="${command}"/>
                    <input class="col-md-2 col-md-offset-1 btn btn-dange searchButton" type="submit" class="tabSub"
                           value="Search"/>
                </div>
            </form>
            <form class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                <div class="col-md-12">
                    <input class="col-md-9 searchDate" type="date" name="date" value="${date}">
                    <input class="col-md-2 col-md-offset-1 btn  searchButton" type="submit">
                </div>
            </form>

            </div>
            <br>
            <div class="col-md-12 row getRidOfpad" style="margin-top: 10px">
                <form  class="col-md-4 col-md-offset-8 getRidOfpad" action="SearchServlet" method="post">
                    <%--<div class="col-md-12">--%>
                        <%--<input class='col-md-3 searchButton' type="submit" name="sortForward" value="Ascending">--%>
                        <%--<input class='col-md-3 col-md-offset-2 searchButton' type="submit" name="sortBackward" value="Descending">--%>
                    <%--</div>--%>
                        <%--<div class="dropdown">--%>
                            <%--<button class="dropbtn">Sort Date by</button>--%>
                            <%--<div class="dropdown-content">--%>
                                <%--<a href="#"><input class='searchButton' type="submit" name="sortForward" value="Ascending"></a>--%>
                                <%--<a href="#"><input class='col-md-offset-2 searchButton' type="submit" name="sortBackward" value="Descending"></a>--%>
                            <%--</div>--%>
                        <%--</div>--%>

                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle dropBtn" type="button" data-toggle="dropdown">Sort Date By
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><input class="sortBtn" type="submit" name="sortForward" value="Descending"></li>
                                <li><input class="sortBtn" type="submit" name="sortBackward" value="Ascending"></li>
                                <%--<li><a href="#">JavaScript</a></li>--%>
                            </ul>
                        </div>


                </form>
            </div>






    </div>
</div>


<div class="">
    <div class="container container-margin">
        <c:choose>
            <c:when test="${fn:length(articles) gt 0}">

                <c:forEach var="article" items="${articles}">
                    <div class="row contentCard02 col-md-12" id="${article.articleId}">
                        <div class="">
                            <div class="innerContent">
                                <h5 class="card-title"> ${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span>
                                    <span>${article.modifiedDateAndTime}</span></p>

                                    <%--only show short content with 200 chars--%>
                                    <%--<hr class="line">--%>
                                <div class="articleContent">
                                    <%--<p>${article.content}</p>--%>
                                        <%--<p>${fn:substring(article.content, 0, 199)}</p>--%>
                                        <%--<p><c:out value="${article.content}" escapeXml="false"/></p>--%>
                                        <%--<span id="${article.articleId}"></span>--%>
                                        <%--<script>--%>
                                        <%--document.getElementById("${article.articleId}").innerText = "<strong>hello</strong>";--%>
                                        <%--</script>--%>
                                </div>

                                <button type="button" class="btn load btn-set col-md-6" data-toggle="modal"
                                        data-target="#articleModal"
                                        data-username="${article.username}" data-content="${article.content}"
                                        data-title="${article.title}"
                                        data-id="${article.articleId}"   onclick="displayNestedComments(${article.articleId}); showDelete()">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                    Load Article
                                </button>
                                    <%--Chinchien view blog--%>
                                <button class="btn load btn-set col-md-6"
                                        onclick="location.href='HomePage?status=${article.username}';"
                                        value="View Blog">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                    View Blog
                                </button>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${username == article.username}">
                                <button type="button" id="deleteButton"
                                        class="btn load col-md-1 btn-delete col-md-offset-11"
                                        onclick="deleteArticle(${article.articleId})"
                                        data-articleID="">
                                    <img src="image/trashbin.png" alt="Technology" style="width:20px;">
                                    delete
                                </button>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <p>No articles!</p>
            </c:otherwise>
        </c:choose>
        <br>
    </div>
</div>


<%@include file="articleModal.jsp" %>
<%@include file="scripts.jsp" %>
<%@include file="nestedCommentModal.jsp" %>

</body>
</html>
